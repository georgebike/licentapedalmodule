#include "HX711.h"
#include <IRremote.h>

#define DOUT  3
#define CLK   2

// infrared transmitter uses pin 5
// hall sensor uses pin 7

IRsend irsend;    // Modified library to use pin 5 for IR Led output
HX711 scale(DOUT, CLK);
float calibration_factor = -11980.f;

unsigned int first_highestValue = 0, second_highestValue = 0, third_highestValue = 0;
unsigned int sendValue;
boolean sendMsg = false;

void setup() {
  Serial.begin(115200);
  
  pinMode(9, OUTPUT); // a digital pin for determining the time between the interrupt and the actual sending of message
  digitalWrite(9, LOW);
  
  pinMode(7, INPUT);
  attachInterrupt(digitalPinToInterrupt(7), hallTrigger, RISING);
  scale.set_gain(64);
  scale.tare(); //Reset the scale to 0
  long zero_factor = scale.read_average(5); //Get a baseline reading
  delay(500);  //Wait half a second for the readings to settle
  scale.set_scale(calibration_factor); //Adjust to this calibration factor
  zero_factor = scale.read_average(5);  // Perform another baseline reading
  scale.set_offset(zero_factor);  //Set the baseline reading as offset
  delay(300);
  scale.tare();
}
void loop() {
  static float value = 0;
  /* check if a hall interrupt has occured and if it did, send the value via infrared transmitter */
  if (sendMsg == true) {
    digitalWrite(9, LOW);
    irsend.sendSony(sendValue, 16);
    delay(10);
    irsend.sendSony(sendValue, 16);
    delay(10);
    sendMsg = false;
  }
  
  float reading = scale.get_units();
  
  if (reading < 0) reading = 0;   // For negative readings, saturate to 0
  if(reading < value || reading - value > 0.15)   // Consider only upward variations of more than 0.15 kg or any downwards vaiations
    value = reading;
  unsigned int convertedValue = (value * 0.453592) * 10;  // Convert lbs to kg and scale it with 10 (1 decimal from float to uint16_t)

  Serial.println(convertedValue);
  
  if (convertedValue > first_highestValue) {
    swapValues();
    first_highestValue = convertedValue;
  }  
  delay(1);
}

void swapValues() {
  third_highestValue = second_highestValue;
  second_highestValue = first_highestValue;
}

void resetValues() {
  first_highestValue = 0;
  second_highestValue = 0;
  third_highestValue = 0;
}

unsigned int averageValues() {
  return (first_highestValue + second_highestValue + third_highestValue) / 3;
}

/* Routine being called every hall sensor interrupt */
void hallTrigger(){
  /* NEVER SEND A MESSAGE WITH IRSEND IN THE INTERRUPT RUTINE. IT WILL CRASH THE ENTIRE PROGRAM !!! */
  sendValue = averageValues();
  sendMsg = true;
  digitalWrite(9, HIGH);
  resetValues();
}
